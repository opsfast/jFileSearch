package net.qqxh.resolve2text.impl;


import net.qqxh.common.utils.TxtCharsetUtil;
import net.qqxh.resolve2text.File2TextResolve;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.io.UnsupportedEncodingException;

@Component
public class Json2TextResolve implements File2TextResolve {
    private static String TYPE = "json";

    @Override
    public String resolve(byte[] file) {
        String text = "";
        try {
            String encoding = TxtCharsetUtil.getCharset(file);
            text = new String(file, encoding);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (IOException e1) {
            e1.printStackTrace();
        }
        return text;
    }

    @Override
    public String getType() {
        return TYPE;
    }
    // 获得文件编码方式

}
